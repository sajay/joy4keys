use gilrs::{Gilrs, Button, Event, EventType, Axis};
use enigo::*;

const MOUSE_SENSITIVITY_L:f32 = 20.0;
const MOUSE_SENSITIVITY_R:f32 = 5.0;

fn main() {
	let mut enigo: enigo::Enigo = Enigo::new();

	let mut ACTIVE_KEY_MAP: [[bool; 8]; 16] = [ [ false; 8 ]; 16 ];

	let mut sleep_level = 0;
	let mut last_event_time: std::time::SystemTime = std::time::SystemTime::now();

	let mut gilrs = Gilrs::new().unwrap();

	let mut axle_lx:f32 = 0.0;
	let mut axle_ly:f32 = 0.0;
	let mut axle_rx:f32 = 0.0;
	let mut axle_ry:f32 = 0.0;

	loop {
		if axle_lx != 0.0 || axle_ly != 0.0 || axle_rx != 0.0 || axle_ry != 0.0 {
			enigo.mouse_move_relative(
				(axle_lx * MOUSE_SENSITIVITY_L + axle_rx * MOUSE_SENSITIVITY_R) as i32,
				-(axle_ly * MOUSE_SENSITIVITY_L + axle_ry * MOUSE_SENSITIVITY_R) as i32
			);

		}
		{
			while let Some(Event { id, event, time }) = gilrs.next_event() {
				last_event_time = time;

				let joy = gilrs.gamepad(id);

				match event {
					EventType::ButtonPressed(x,_y) => {
						let mod0 = joy.is_pressed(Button::LeftTrigger);
						let mod1 = joy.is_pressed(Button::RightTrigger);	
						let mod2 = joy.is_pressed(Button::LeftTrigger2);
						let mod3 = joy.is_pressed(Button::RightTrigger2);

						match x {
							Button::DPadUp =>    push_key(&mut enigo, &mut ACTIVE_KEY_MAP, 0, mod0, mod1, mod2, mod3),
							Button::DPadLeft =>  push_key(&mut enigo, &mut ACTIVE_KEY_MAP, 1, mod0, mod1, mod2, mod3),
							Button::DPadRight => push_key(&mut enigo, &mut ACTIVE_KEY_MAP, 2, mod0, mod1, mod2, mod3),
							Button::DPadDown =>  push_key(&mut enigo, &mut ACTIVE_KEY_MAP, 3, mod0, mod1, mod2, mod3),
							Button::North =>     push_key(&mut enigo, &mut ACTIVE_KEY_MAP, 4, mod0, mod1, mod2, mod3),
							Button::West =>      push_key(&mut enigo, &mut ACTIVE_KEY_MAP, 5, mod0, mod1, mod2, mod3),
							Button::East =>      push_key(&mut enigo, &mut ACTIVE_KEY_MAP, 6, mod0, mod1, mod2, mod3),
							Button::South =>     push_key(&mut enigo, &mut ACTIVE_KEY_MAP, 7, mod0, mod1, mod2, mod3),
							Button::Select => enigo.key_down(Key::Control),
							Button::Start => enigo.key_down(Key::Shift),
							Button::Mode => enigo.key_down(Key::Meta),
							Button::LeftThumb => enigo.mouse_down(MouseButton::Left),
							Button::RightThumb => enigo.mouse_down(MouseButton::Right),
							_ => {},
						}
					},
					EventType::ButtonReleased(x, _y) => {
						match x {
							Button::DPadUp =>    release_key(&mut enigo, &mut ACTIVE_KEY_MAP, 0),
							Button::DPadLeft =>  release_key(&mut enigo, &mut ACTIVE_KEY_MAP, 1),
							Button::DPadRight => release_key(&mut enigo, &mut ACTIVE_KEY_MAP, 2),
							Button::DPadDown =>  release_key(&mut enigo, &mut ACTIVE_KEY_MAP, 3),
							Button::North =>     release_key(&mut enigo, &mut ACTIVE_KEY_MAP, 4),
							Button::West =>      release_key(&mut enigo, &mut ACTIVE_KEY_MAP, 5),
							Button::East =>      release_key(&mut enigo, &mut ACTIVE_KEY_MAP, 6),
							Button::South =>     release_key(&mut enigo, &mut ACTIVE_KEY_MAP, 7),
							Button::Select => enigo.key_up(Key::Control),
							Button::Start => enigo.key_up(Key::Shift),
							Button::Mode => enigo.key_up(Key::Meta),
							Button::LeftThumb => enigo.mouse_up(MouseButton::Left),
							Button::RightThumb => enigo.mouse_up(MouseButton::Right),
							_ => {},
						}	
					},
					EventType::AxisChanged(_x,_y,_z) => {
						axle_lx = joy.value(Axis::LeftStickX);
						axle_ly = joy.value(Axis::LeftStickY);
						axle_rx = joy.value(Axis::RightStickX);
						axle_ry = joy.value(Axis::RightStickY);
					},
					_ => (),
				}
			}
		}
		if axle_lx != 0.0 || axle_ly != 0.0 || axle_rx != 0.0 || axle_ry != 0.0 {
			enigo.mouse_move_relative(
				(axle_lx * MOUSE_SENSITIVITY_L + axle_rx * MOUSE_SENSITIVITY_R) as i32,
				-(axle_ly * MOUSE_SENSITIVITY_L + axle_ry * MOUSE_SENSITIVITY_R) as i32
			);
			std::thread::sleep(std::time::Duration::from_millis(20));
		}
		else {
			std::thread::sleep(std::time::Duration::from_millis(100));
		}
	}
}

const KEY_MAP: [[Key; 8]; 16] = [
	[ Key::Layout('.'),	Key::Tab,		Key::Layout('e'),	Key::Layout('t'),	Key::Delete,		Key::Backspace,		Key::Return,		Key::Space	],
	[ Key::Layout('a'),	Key::Layout('o'),	Key::Layout('i'),	Key::Layout('n'),	Key::Layout('s'),	Key::Layout('r'),	Key::Layout('h'),	Key::Layout('d')],
	[ Key::Layout('l'),	Key::Layout('u'),	Key::Layout('c'),	Key::Layout('m'),	Key::Layout('f'),	Key::Layout('y'),	Key::Layout('w'),	Key::Layout('g')],
	[ Key::Layout('p'),	Key::Layout('b'),	Key::Layout('v'),	Key::Layout('k'),	Key::Layout('x'),	Key::Layout('q'),	Key::Layout('j'),	Key::Layout('z')],

	[ Key::Layout('0'),	Key::Layout('1'),	Key::Layout('2'),	Key::Layout('3'),	Key::Layout('4'),	Key::Layout('5'),	Key::Layout('6'),	Key::Layout('7')],
	[ Key::Layout('8'),	Key::Layout('9'),	Key::Layout('@'),	Key::Layout('#'),	Key::Layout('$'),	Key::Layout('%'),	Key::Layout('^'),	Key::Layout('&')],	
	[ Key::Layout('\\'),	Key::Layout(' '),	Key::Layout('~'),	Key::Layout('='),	Key::Layout('+'),	Key::Layout('-'),	Key::Layout('*'),	Key::Layout('/')],	
	[ Key::Layout(','),	Key::Layout(':'),	Key::Layout(';'),	Key::Layout('\''),	Key::Layout('"'),	Key::Layout('?'),	Key::Layout('!'),	Key::Layout('.')],	

	[ Key::UpArrow,		Key::LeftArrow,		Key::RightArrow,	Key::DownArrow,		Key::PageUp,		Key::Home,		Key::End,		Key::PageDown	],
	[ Key::Layout('_'),	Key::Layout('`'),	Key::F5,		Key::F5,		Key::F5,		Key::F5,		Key::F5,			Key::F5		],
	[ Key::Layout('<'),	Key::Layout('>'),	Key::Layout('('),	Key::Layout(')'),	Key::Layout('{'),	Key::Layout('}'),	Key::Layout('['),	Key::Layout(']')],
	[ Key::Backspace; 8],

	[ Key::Backspace; 8],
	[ Key::Backspace; 8],
	[ Key::Backspace; 8],
	[ Key::Backspace; 8],
];


fn push_key(enigo: &mut enigo::Enigo, ACTIVE_KEY_MAP: &mut [[bool; 8]; 16], button_id:usize, mod0: bool, mod1: bool, mod2: bool, mod3: bool) {
	let mode:usize = (mod0 as u8 * 1 + mod1 as u8 * 2 + mod2 as u8 * 4 + mod3 as u8 * 8) as usize;
	enigo.key_down(KEY_MAP[mode][button_id]);
	ACTIVE_KEY_MAP[mode][button_id] = true;
}

fn release_key(enigo: &mut enigo::Enigo, ACTIVE_KEY_MAP: &mut [[bool; 8]; 16], button_id:usize){
	for i in 0..15 {
		if ACTIVE_KEY_MAP[i][button_id] {
			enigo.key_up(KEY_MAP[i][button_id]);
			ACTIVE_KEY_MAP[i][button_id] = false;
		}
	}
}
